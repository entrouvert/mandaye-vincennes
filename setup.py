#! /usr/bin/env python

'''
   Setup script for Vincennes RP
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

install_requires=[
        'gunicorn>=0.17',
        'mandaye>=0.11',
        'pycrypto>=2.6',
        'BeautifulSoup>=3.1'
]

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    import mandaye_vincennes
    return mandaye_vincennes.__version__

setup(name="mandaye-vincennes",
      version=get_version(),
      license="AGPLv3 or later",
      description="Vincennes rp is a Mandaye project, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Jerome Schneider",
      maintainer_email="jschneider@entrouvert.com",
      scripts=['manager.py', 'server.py'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires
)

