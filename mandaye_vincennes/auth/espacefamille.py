
import copy
import urllib
import socket
import ssl

from mandaye.auth.saml2 import SAML2Auth
from mandaye.http import HTTPRequest
from mandaye.response import _500, _302, _401
from mandaye.server import get_response

class EspaceFamilleAuth(SAML2Auth):

    def replay(self, env, post_values):
        """ This hack a hack method
        There is a bug in httplib so this method make a manual post
        """
        post_values = copy.deepcopy(post_values)
        if not "://" in self.login_url:
            self.login_url = env['target'].geturl() + '/' + self.login_url
        request = HTTPRequest()
        login = get_response(env, request, self.login_url)
        action = self.form_values.get('post_url')
        if login.code == 502:
            return login
        try:
            fsession = login.cookies['JSESSIONID'].value
        except Exception, e:
            return _500('Espace famille: no cookie JSESSIONID', e)

        post_values['idSession'] = fsession
        body = urllib.urlencode(post_values)
        headers = "POST %s HTTP/1.1\r\nAccept-Encoding: identity\r\n\
Content-Length: %d\r\nConnection: close\r\n\
Accept: text/html; */*\r\nHost: %s\r\n\
Cookie: %s\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\n" % (
        action, len(body), env['target'].netloc,
        login.cookies.output(attrs=[], sep=';', header=''))

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ssl_sock = ssl.wrap_socket(s)
        # TODO: don't force the port
        ssl_sock.connect((env['target'].netloc, 443))
        # Set a simple HTTP request -- use httplib in actual code.
        ssl_sock.write(headers)
        ssl_sock.write(body)
        # Read a chunk of data.  Will not necessarily
        # read all the data returned by the server.
        data = ssl_sock.read()
        # note that closing the SSLSocket will also close the underlying socket
        ssl_sock.close()
        data = data.split('\r\n')
        line1 = data[0].split(' ')
        code = line1[1]
        if code == '302':
            del data[0]
            for header in data:
                header = header.split(':', 1)
                if header[0].lower() == 'location':
                    location = header[1].replace(' ', '')
                    location = location.replace(env['target'].geturl(),
                            "%s://%s" % (env['mandaye.scheme'], env["HTTP_HOST"]))
            return _302(location, login.cookies)
        return _401('Login failed')

