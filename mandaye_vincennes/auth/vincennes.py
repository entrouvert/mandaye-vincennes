""" Vincennes authentifications
"""

# TODO : port this code to support the new sql backend

import base64
import traceback

from Crypto.Cipher import AES
from datetime import datetime, timedelta
from urlparse import parse_qs

from mandaye.auth.saml2 import SAML2Auth
from mandaye.log import logger
from mandaye.models import ServiceProvider, SPUser, IDPUser
from mandaye.db import sql_session
from mandaye.response import _502, _302
from mandaye.server import get_response

from mandaye_vincennes import config

class VincennesAuth(SAML2Auth):
    """ Specific authentification class for Vincennes
    """

    pass
