
from mandaye import config
from mandaye.auth.saml2 import END_POINTS_PATH
from mandaye_vincennes.filters import vincennes

form_values = {
        'post_url': '/sezhame/page/connexion-abonne?destination=user',
        'login_url': '/sezhame/page/connexion-abonne',
        'form_attrs': { 'id': 'dk-opac15-login-form', },
        'post_fields': ['user', 'password'],
        'username_field': 'user',
        'password_field': 'password',
        }

filters = vincennes.Biblio()

urls = {
        'login_url': '/mandaye/login',
        'connection_url': '/mandaye/sso',
        'associate_url': '/mandaye/associate',
        'disassociate_url': '/mandaye/disassociate',
        'disassociate_next_url': '/sezhame'
        }

replay_condition = lambda env, response: response.code==302

mapping = [
        {
            'path': r'/(?!/*mandaye)',
            'on_response': [
                {
                    'filter': filters.resp_html,
                    'content-types': ['application/x-javascript', 'text/html', 'text/css'],
                    'decompress': False,
                    },
                ],
            },
        {
            'path': r'/sezhame',
            'on_response': [{
                'filter': filters.resp_multicompte_html,
                'content-types': ['text/html'],
                'values': {
                    'associate_url': '/mandaye/associate',
                    'template': 'biblio/multicompte.html',
                    'nosso_template': 'biblio/nosso.html',
                    'site_name': 'biblio'
                    },
                },]
            },
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                }
            },
        {
            'path': r'/mandaye/sso$',
            'method': 'GET',
            'response': {'auth': 'sso',}
            },
        {
            'path': r'/mandaye/slo$',
            'method': 'GET',
            'response': {'auth': 'slo',}
            },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {
                    'auth': 'single_sign_on_post',
                    'values': {
                        'next_url': '/sezhame'
                        }
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': config.template_vars.get('wcs_url')
                        }
                    }
                },
        {
                'path': r'/mandaye/associate$',
                'method': 'GET',
                'target': '/sezhame/page/connexion-abonne',
                'on_response': [{
                    'filter': filters.resp_associate_login,
                    'values': {
                        'action': '/mandaye/associate',
                        'template': 'biblio/associate.html',
                        'badlogin_msg': "Num&eacute;ro de carte ou/et mot de passe incorrects",
                        'failed_msg': "Vos identifiants pour la biblioth&egrave;que ne fonctionnent plus ! Merci de les ressaisir.",
                        },
                    },
                    {
                        'filter': filters.resp_multicompte_html,
                        'values': {
                            'associate_url': '/mandaye/associate',
                            'template': 'biblio/multicompte.html',
                            'nosso_template': 'biblio/nosso.html',
                            'site_name': 'biblio'
                            },

                        },]
                    },
        {
                'path':  r'/mandaye/associate$',
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    }
                },
        {
                'path': r'/sezhame/page/connexion-abonne$',
                'method': 'GET',
                'on_response': [{
                    'filter': filters.resp_html_login_page,
                    'values': {
                        'connection_url': '/mandaye/sso',
                        'template': 'biblio/login.html',
                        'title': 'Connexion via votre compte citoyen'
                        }
                    }]
                },
        {
                'path': r'/mandaye/change_user$',
                'method': 'GET',
                'response': {'auth': 'change_user',}
                },
        ]

