
from mandaye import config
from mandaye.auth.saml2 import END_POINTS_PATH
from mandaye_vincennes.filters import vincennes

filters = vincennes.Duonet()
duonet_key = 'CV4j27Em0dM%3d'

form_values = {
        'login_url': 'Connect.aspx?key=%s' % duonet_key,
        'form_attrs': {'name': 'form1'},
        'post_fields': ['txtNomFoyer', 'txtDateNaissance', 'txtCode'],
        'username_field': 'txtNomFoyer',
        'password_field': 'txtCode'
        }

urls = {
        'login_url': '/vincennes/login',
        'connection_url': '/mandaye/sso',
        'associate_url': '/associate',
        'disassociate_url': '/mandaye/disassociate',
        'disassociate_next_url': '/'
        }

replay_condition = lambda env, response: response.code==302

mapping = [
        {
            'path': r'/',
            'on_response': [
                {
                    'content-types': ['text/html'],
                    'filter': filters.resp_global_html,
                    'values': {
                        'associate_url': '/associate',
                        'nosso_template': 'duonet/nosso.html',
                        'site_name': 'duonet'
                        },
                    },
                ]
            },
        {
            'path': r'/vincennes/login$',
            'method': 'GET',
            'response': {
                'auth':'login',
                }
            },
        {
            'path': r'/associate$',
            'method': 'GET',
            'target': '/Connect.aspx?key=%s' % duonet_key,
            'on_response': [{
                'filter': filters.resp_associate,
                'values': {
                    'action': '/associate',
                    'template': 'duonet/associate.html',
                    'badlogin_msg': "Mauvais identifiants",
                    'failed_msg': "Vos identifiants pour le conservatoire ne fonctionnent plus ! Merci de les ressaisir.",
                    },
                }]
            },
        {
            'path':  r'/associate$',
            'method': 'POST',
            'response': {
                'auth': 'associate_submit',
                }
            },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {
                    'auth': 'single_sign_on_post',
                    'values': {
                        'login_url': '/vincennes/login',
                        'next_url': '/Default.aspx'
                        }
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': config.template_vars.get('wcs_url')
                        }
                    }
                },
        {
                'path': r'/mandaye/sso$',
                'method': 'GET',
                'response': {'auth': 'sso'}
                },
        {
                'path': r'/mandaye/slo$',
                'method': 'GET',
                'response': {'auth': 'slo'}
                },
        {
                'path': r'/Connect.aspx$',
                'method': 'GET',
                'on_response': [{
                    'filter': filters.resp_login_page,
                    'values': {
                        'connection_url': '/mandaye/sso',
                        'template': 'duonet/login.html',
                        }
                    }]
                },
        {
                'path': r'/$',
                'method': 'GET',
                'redirect': '/Connect.aspx?key=%s' % duonet_key,
                },
        ]


