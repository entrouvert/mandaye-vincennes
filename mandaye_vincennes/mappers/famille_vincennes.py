
from mandaye import config
from mandaye.auth.saml2 import END_POINTS_PATH
from mandaye_vincennes.filters import vincennes

folder_target = '/vincennes-noredirect'
filters = vincennes.EspaceFamille()

form_values = {
        'post_url': '%s/login.do' % folder_target,
        'login_url': '%s/index.do' % folder_target,
        'form_attrs': { 'action': 'login.do', },
        'post_fields': ['codeFamille', 'motDePasse'],
        'username_field': 'codeFamille',
        'password_field': 'motDePasse'
        }

urls = {
        'login_url': '/mandaye/login',
        'associate_url': '%s/associate' % folder_target,
        'connection_url': '/mandaye/sso',
        'disassociate_url': '/mandaye/disassociate',
        'disassociate_next_url': '%s/deconnexion.do' % folder_target
        }

replay_condition = lambda env, response: response.code==302

mapping = [
        {
            'path': r'/$',
            'method': 'GET',
            'redirect': '%s/affichage_accueil_prive.do' % folder_target
            },
        {
            'path': r"/(?!.*/associate)",
            'on_response': [{
                'filter': filters.resp_login_page,
                'content-types': ['text/html'],
                'values': {
                    'connection_url': '/mandaye/sso',
                    'template': 'famille/login.html',
                    'title': 'Connexion via votre compte citoyen'
                    }
                },]
            },
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                },
            },
        {
            'path': r'%s/associate$' % folder_target,
            'method': 'GET',
            'target': '%s/index.do' % folder_target,
            'on_response': [{
                'filter': filters.resp_associate,
                'values': {
                    'action': '%s/associate' % folder_target,
                    'template': 'famille/associate.html',
                    'title': 'Associer un compte au compte citoyen de Vincennes',
                    'badlogin_msg': "Code famille et/ou mot de passe incorrects",
                    'failed_msg': "Vos identifiants pour l'espace famille ne fonctionnent plus ! Merci de les ressaisir.",
                    },
                }]
            },
        {
                'path':  r'%s/associate$' % folder_target,
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    },
                },
        {
                'path': r'/mandaye/sso$',
                'method': 'GET',
                'response': {
                    'auth': 'sso',
                    'values': {
                        'next_url': '/mandaye/login',
                        }
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {
                    'auth': 'single_sign_on_post',
                    'values': {
                        'login_url': '/mandaye/login',
                        }
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': config.template_vars.get('wcs_url')
                        }
                    }
                },
        ]

